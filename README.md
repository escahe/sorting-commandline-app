# SortingApp

command line java app

run `mvn clean package` to generate the jar  file

run `java -jar target/sorting-command-line-app-1.0-SNAPSHOT.jar x x x x ` Replacing the 'x' by integer numbers

example:

```bash
$ java -jar target/sorting-command-line-app-1.0-SNAPSHOT.jar 112 1234 12312 123124 12 1 5
sorted numbers: [1, 5, 12, 112, 1234, 12312, 123124]
```
