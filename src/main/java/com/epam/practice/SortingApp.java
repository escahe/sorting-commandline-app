package com.epam.practice;

import java.util.Arrays;
import java.util.Scanner;

public class SortingApp {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        try {

            int[] sortedArr = sortedIntArray(args);
            System.out.println("sorted numbers: " + Arrays.toString(sortedArr));
            
            System.out.println("Do you want to save the numbers in file? (if yes: press enter, if no: type n and press enter)");
            String answer = sc.nextLine();
            sc.close();
            if(answer.isEmpty())
                if(Save.saveIntArrayToFile(sortedArr))
                    System.out.println("Succesfuly saved in " + Save.FILE_NAME);
                else
                    System.out.println("Something wrong");
            
        } catch (NumberFormatException e) {

            System.err.println("Invalid integer format.\nallowed numbers range: [" + Integer.MIN_VALUE + ", " + Integer.MAX_VALUE + "]");
            System.err.println("bad format e.g. 'one two tree', '1 2 3L', '2147483648 -2147483649', '1.4 1.1'");
        
        } catch (IllegalArgumentException e) {
            System.err.println(e.getMessage());
        }

    }

    public static int[] sortedIntArray(String[] args) {
        if(args.length == 0) throw new IllegalArgumentException("No args detected, args e.g. 3 2 1");
        if(args.length > 10) throw new IllegalArgumentException("Up to 10 args are allowed to be sorted");
        int[] intArr = new int[args.length];
        for (int i = 0; i < intArr.length; i++) {
            intArr[i] = Integer.parseInt(args[i]);
        }
        Arrays.sort(intArr);
        return intArr;

    }
}