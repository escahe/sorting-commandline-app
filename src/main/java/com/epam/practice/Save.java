package com.epam.practice;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.apache.commons.io.FileUtils;

public class Save {
    private Save(){}
    public final static String FILE_NAME = "sorted_numbers.txt";
    public static boolean saveIntArrayToFile(int[] numbers) {
        try {
            File file = new File(FILE_NAME);
            FileUtils.writeLines(file, Arrays.asList(Arrays.toString(numbers)));
            return true;
        } catch (IOException e) {
            return false;
        }
    }
}
