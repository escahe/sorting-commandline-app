import org.junit.Test;
import com.epam.practice.SortingApp;

public class SortingAppNoArgsTest {
    @Test(expected = IllegalArgumentException.class)
    public void testNoArgsCase(){
        SortingApp.sortedIntArray(new String[]{});
    }
}
