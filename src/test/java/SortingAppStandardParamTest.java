import static org.junit.Assert.assertArrayEquals;
import static com.epam.practice.SortingApp.sortedIntArray;

import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class SortingAppStandardParamTest {
    String[] args;
    int[] expected;

    public SortingAppStandardParamTest(String[] args, int[] expected) {
        this.args = args;
        this.expected = expected;
    }

    @Parameters
    public static Collection<Object[]> testParams(){
        return List.of(
            new Object[][]{
                {new String[]{"5","4","3","2","1"}, new int[]{1,2,3,4,5}},
                {new String[]{"500","444","333","202","101"}, new int[]{101,202,333,444,500}},
                {new String[]{"10","5"}, new int[]{5,10}},
                {new String[]{"5","4","3","2","1","10","9","8","7","6"}, new int[]{1,2,3,4,5,6,7,8,9,10}},
                {new String[]{Integer.MAX_VALUE+"", Integer.MIN_VALUE+""}, new int[]{Integer.MIN_VALUE, Integer.MAX_VALUE}},
            }
        );
    }

    @Test
    public void testStandardParamsCase(){
        int[] actual = sortedIntArray(args);
        assertArrayEquals(expected, actual);
    }
}
