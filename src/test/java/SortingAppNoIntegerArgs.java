import static com.epam.practice.SortingApp.sortedIntArray;

import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class SortingAppNoIntegerArgs {
    String[] args;

    public SortingAppNoIntegerArgs(String[] args) {
        this.args = args;
    }

    @Parameters
    public static Collection<Object[]> testParams(){
        return List.of(
            new Object[][]{
                {new String[]{"5","4","3","2","A"}},
                {new String[]{"Two", "One"}},
                {new String[]{"1.5", "1.4","1.3","1.2"}},
                {new String[]{"First", "Last", "Middle"}},
            }
        );
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNoIntegerArgsCase(){
        sortedIntArray(args);
    }
}
