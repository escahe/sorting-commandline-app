import org.junit.Test;
import com.epam.practice.SortingApp;

public class SortingAppMoreThanTen {
    @Test(expected = IllegalArgumentException.class)
    public void testMoreThanTenArgs(){
        SortingApp.sortedIntArray(new String[]{"10","9","8","7","6","5","4","3","2","1","0"});
    }
}
